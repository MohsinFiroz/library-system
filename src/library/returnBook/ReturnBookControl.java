package library.returnBook;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

    private ReturnBookUI bookUI;

    private enum ControlState {INITIALISED, READY, INSPECTING}

	private ControlState state;

    private Library library;
    private Loan currentLoan;

    public ReturnBookControl() {
        this.library = Library.getInstance();
        state = ControlState.INITIALISED;
    }

    public void setUi(ReturnBookUI ui) {
        if (!state.equals(ControlState.INITIALISED))
            throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");

        this.bookUI = ui;
        ui.setState(ReturnBookUI.UiState.READY);
        state = ControlState.READY;
    }

    public void bookScanned(int bookId) {
        if (!state.equals(ControlState.READY))
            throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");

        Book currentBook = library.getBook(bookId);

        if (currentBook == null) {
            bookUI.display("Invalid Book Id");
            return;
        }
        if (!currentBook.isOnLoan()) {
            bookUI.display("Book has not been borrowed");
            return;
        }
        currentLoan = library.getLoanByBookId(bookId);
        double overDueFine = 0.0;
        if (currentLoan.isOverDue())
            overDueFine = library.calculateOverDueFine(currentLoan);

        bookUI.display("Inspecting");
        bookUI.display(currentBook.toString());
        bookUI.display(currentLoan.toString());

        if (currentLoan.isOverDue())
            bookUI.display(String.format("\nOverdue fine : $%.2f", overDueFine));

        bookUI.setState(ReturnBookUI.UiState.INSPECTING);
        state = ControlState.INSPECTING;
    }

    public void scanningComplete() {
        if (!state.equals(ControlState.READY))
            throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");

        bookUI.setState(ReturnBookUI.UiState.COMPLETED);
    }

    public void dischargeLoan(boolean isDamaged) {
        if (!state.equals(ControlState.INSPECTING))
            throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");

        library.dischargeLoan(currentLoan, isDamaged);
        currentLoan = null;
        bookUI.setState(ReturnBookUI.UiState.READY);
        state = ControlState.READY;
    }

}
