package library.returnBook;

import java.util.Scanner;

public class ReturnBookUI {

    public static enum UiState {INITIALISED, READY, INSPECTING, COMPLETED}

    private ReturnBookControl returnBookControl;
    private Scanner input;
    private UiState state;

    public ReturnBookUI(ReturnBookControl bookControl) {
        this.returnBookControl = bookControl;
        input = new Scanner(System.in);
        state = UiState.INITIALISED;
        bookControl.setUi(this);
    }

    public void run() {
        output("Return Book Use Case UI\n");

        while (true) {

            switch (state) {

                case INITIALISED:
                    break;

                case READY:
                    String bookInputString = input("Scan Book (<enter> completes): ");
                    if (bookInputString.length() == 0)
                        returnBookControl.scanningComplete();

                    else {
                        try {
                            int Book_Id = Integer.valueOf(bookInputString).intValue();
                            returnBookControl.bookScanned(Book_Id);
                        } catch (NumberFormatException e) {
                            output("Invalid bookId");
                        }
                    }
                    break;

                case INSPECTING:
                    String answer = input("Is book damaged? (Y/N): ");
                    boolean isDamaged = false;
                    if (answer.toUpperCase().equals("Y"))
                        isDamaged = true;

                    returnBookControl.dischargeLoan(isDamaged);

                case COMPLETED:
                    output("Return processing complete");
                    return;

                default:
                    output("Unhandled state");
                    throw new RuntimeException("ReturnBookUI : unhandled state :" + state);
            }
        }
    }

    private String input(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }

    private void output(Object object) {
        System.out.println(object);
    }

    public void display(Object object) {
        output(object);
    }

    public void setState(UiState state) {
        this.state = state;
    }

}
